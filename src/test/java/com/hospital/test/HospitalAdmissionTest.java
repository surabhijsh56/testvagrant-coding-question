package com.hospital.test;

import com.hospital.Hospital;
import com.hospital.HospitalReception;
import com.hospital.patient.Patient;
import com.hospital.patient.PatientAdmission;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

public class HospitalAdmissionTest {

    private static final String BENGALURU_CITY = "Bengaluru";
    private static final String MUMBAI_CITY = "Mumbai";

    private Hospital hospital;
    private HospitalReception hospitalReception;

    @Before
    public void init() {
        hospital = new Hospital("Test Hospital", BENGALURU_CITY);
        hospitalReception = new HospitalReception(hospital);

        Patient patient1 = new Patient("Bangalore Local 1", 22, BENGALURU_CITY);
        //Register today
        hospitalReception.addPatient(new PatientAdmission(patient1, LocalDate.now()));

        Patient patient2 = new Patient("Bangalore Local 3", 22, BENGALURU_CITY);
        hospitalReception.addPatient(new PatientAdmission(patient2, LocalDate.now()));

        Patient patient3 = new Patient("Mumbai Local 1", 32, MUMBAI_CITY);
        hospitalReception.addPatient(new PatientAdmission(patient3, LocalDate.now()));

        Patient patient4 = new Patient("Mumbai Local 2", 42, MUMBAI_CITY);
        hospitalReception.addPatient(new PatientAdmission(patient4, LocalDate.now()));
    }

    @Test
    public void testAbstractMethodForPercentage() {
        Assert.assertEquals(hospitalReception.getOutStationPercentageFor(LocalDate.now()), 50f, 0.1);
    }

    @Test
    public void testAbstractMethodForPercentageOther() {
        // Add another patient from Mumbai
        Patient patient = new Patient("Mumbai Local 2", 42, MUMBAI_CITY);
        hospitalReception.addPatient(new PatientAdmission(patient, LocalDate.now()));
        // 3 OutStation / 5 total = 60%
        Assert.assertEquals(hospitalReception.getOutStationPercentageFor(LocalDate.now()), 60f, 0.1);
    }

    @Test
    public void completeTest() {
        List<PatientAdmission> admissions = hospitalReception.getPatientAdmissionsFor(LocalDate.now());
        Assert.assertEquals(admissions.size(), 4);

        admissions = hospitalReception.getPatientAdmissions();
        Assert.assertEquals(admissions.size(), 4);

        admissions = hospitalReception.getOutStationAdmissionsFor(LocalDate.now());
        Assert.assertEquals(admissions.size(), 2);

        List<PatientAdmission> localAdmissions = hospitalReception.getOutStationAdmissionsFor(LocalDate.now());
        Assert.assertEquals(localAdmissions.size(), 2);

        List<PatientAdmission> totalAdmissionsForToday = hospitalReception.getPatientAdmissionsFor(LocalDate.now());
        Assert.assertEquals(totalAdmissionsForToday.size(), 4);

        float percentageLocal = ((float) localAdmissions.size() / totalAdmissionsForToday.size()) * 100;
        Assert.assertEquals(percentageLocal, 50f, 0.1);
    }


    @Test
    public void testPercentage() {
        // Get admissions for today
        List<PatientAdmission> admissions = hospitalReception.getPatientAdmissionsFor(LocalDate.now());

        int localAdmission = 0;
        for (PatientAdmission patientAdmission : admissions) {
            if (hospitalReception.isOutStationAdmission(patientAdmission)) {
                localAdmission++;
            }
        }

        float percentageLocal = ((float) localAdmission / admissions.size()) * 100;
        Assert.assertEquals(percentageLocal, 50f, 0.1);
    }


    @Test
    public void testTotal() {
        List<PatientAdmission> admissions = hospitalReception.getPatientAdmissionsFor(LocalDate.now());
        Assert.assertEquals(admissions.size(), 4);
    }

    @Test
    public void testOldAdmission() {
        // Add old patient
        hospitalReception.addPatient(new PatientAdmission(new Patient("test", 11, MUMBAI_CITY), LocalDate.of(2018, 11, 1)));
        List<PatientAdmission> admissions = hospitalReception.getPatientAdmissions();
        Assert.assertEquals(admissions.size(), 5);

        // Todays admission should still be same
        admissions = hospitalReception.getPatientAdmissionsFor(LocalDate.now());
        Assert.assertEquals(admissions.size(), 4);
    }
}
