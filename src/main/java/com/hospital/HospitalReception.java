package com.hospital;

import com.hospital.patient.PatientAdmission;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HospitalReception {

    private Hospital hospital;
    private List<PatientAdmission> patientAdmissions = new ArrayList<>();

    public HospitalReception(Hospital hospital) {
        this.hospital = hospital;
    }

    public void addPatient(PatientAdmission patientAdmission) {
        patientAdmissions.add(patientAdmission);
    }

    public List<PatientAdmission> getPatientAdmissions() {
        return patientAdmissions;
    }

    public float getOutStationPercentageFor(LocalDate targetDate) {
        List<PatientAdmission> OutStationAdmissions = getOutStationAdmissionsFor(targetDate);
        List<PatientAdmission> totalAdmissionsForToday = getPatientAdmissionsFor(targetDate);

        return ((float) OutStationAdmissions.size() / totalAdmissionsForToday.size()) * 100;
    }


    public List<PatientAdmission> getOutStationAdmissionsFor(LocalDate targetDate) {
        List<PatientAdmission> outStationAdmissions = new ArrayList<>();

        for (PatientAdmission patientAdmission : patientAdmissions) {
            if (patientAdmission.getEnrollmentDate().equals(targetDate) && isOutStationAdmission(patientAdmission)) {
                outStationAdmissions.add(patientAdmission);
            }
        }
        return outStationAdmissions;
    }

    public boolean isOutStationAdmission(PatientAdmission patientAdmission) {
        return !patientAdmission.getPatient().getCity().equals(hospital.getCity());
    }

    public List<PatientAdmission> getPatientAdmissionsFor(LocalDate targetDate) {
        List<PatientAdmission> admissions = new ArrayList<>();

        for (PatientAdmission patientAdmission : patientAdmissions) {
            if (patientAdmission.getEnrollmentDate().equals(targetDate)) {
                admissions.add(patientAdmission);
            }
        }
        return admissions;
    }
}
