package com.hospital;

public class Hospital {

    private String name;
    private String city;

    public Hospital(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }
}
