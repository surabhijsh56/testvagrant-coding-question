package com.hospital.patient;

import java.time.LocalDate;

public class PatientAdmission {

    private Patient patient;
    private LocalDate enrollmentDate;

    public PatientAdmission(Patient patient, LocalDate enrollmentDate) {
        this.patient = patient;
        this.enrollmentDate = enrollmentDate;
    }

    public LocalDate getEnrollmentDate() {
        return enrollmentDate;
    }

    public Patient getPatient() {
        return patient;
    }
}
